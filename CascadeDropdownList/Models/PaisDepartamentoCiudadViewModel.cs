﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CascadeDropdownList.Models
{
    public class PaisDepartamentoCiudadViewModel
    {
        public int PaisId { get; set; }
        public int DepartamentoId { get; set; }
        public int CiudadId { get; set; }
    }
}