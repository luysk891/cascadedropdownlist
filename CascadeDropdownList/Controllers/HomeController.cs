﻿using CascadeDropdownList.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CascadeDropdownList.Controllers
{
    public class HomeController : Controller
    {
        dropdownlistEntities db = new dropdownlistEntities();

        public ActionResult Index()
        {
            List<pais> PaisList = db.pais.ToList();
            ViewBag.PaisList = new SelectList(PaisList,"id","nombre");
            return View();
        }

        public JsonResult GetDepartamentoList(int PaisId) {
            db.Configuration.ProxyCreationEnabled = false;
            List<departamento> DepartamentoList = db.departamento.Where(x=>x.id_pais== PaisId).ToList();
            return Json(DepartamentoList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCiudadList(int DepartamentoId)
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<ciudad> CiudadList = db.ciudad.Where(x => x.id_departamentoo == DepartamentoId).ToList();
            return Json(CiudadList, JsonRequestBehavior.AllowGet);
        }

    }
}